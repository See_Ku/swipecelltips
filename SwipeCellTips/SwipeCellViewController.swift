//
//  SwipeCellViewController.swift
//  SwipeCellTips
//
//  Created by See.Ku on 2014/09/28.
//  Copyright (c) 2014 AxeRoad. All rights reserved.
//

import UIKit

class SwipeCellViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

	@IBOutlet weak var swipeTable: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

		// delegateとdataSourceを設定
		swipeTable.delegate = self
		swipeTable.dataSource = self

		// 編集と追加のボタンを設定
		let edit = editButtonItem()
		let add = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: "insertNewItem")
		self.navigationItem.rightBarButtonItems = [edit, add]

		// 予めいくつかアイテムを追加しておく
		insertNewItem()
		insertNewItem()
		insertNewItem()
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
	////////////////////////////////////////////////////////////////

	var itemArray = [String]()
	var itemCount = 1

	// アイテムを追加
	func insertNewItem() {
		let str = "Item No.\(itemCount++)"
		itemArray.insert(str, atIndex: 0)

		let indexPath = NSIndexPath(forRow: 0, inSection: 0)
		swipeTable.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
	}

	// 編集操作を自分のTableViewに反映させる
	override func setEditing(editing: Bool, animated: Bool) {
		super.setEditing(editing, animated: animated)

		swipeTable.setEditing(editing, animated: animated)
	}

	////////////////////////////////////////////////////////////////
	// MARK: - Cellの編集

	func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
		return true
	}

	func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
		return true
	}

	func tableView(tableView: UITableView, moveRowAtIndexPath sourceIndexPath: NSIndexPath, toIndexPath destinationIndexPath: NSIndexPath) {
		let tmp = itemArray[sourceIndexPath.row]
		itemArray.removeAtIndex(sourceIndexPath.row)
		itemArray.insert(tmp, atIndex: destinationIndexPath.row)
	}

	// 編集操作に対応
	// ※スワイプで処理する場合、ここでは何もしないが関数は必要
	func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
	}

	// スワイプで出てくるコマンドを設定
	func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [AnyObject]? {

		// 編集
		let edit = UITableViewRowAction(style: .Normal, title: "Edit") {
			(action, indexPath) in

			self.itemArray[indexPath.row] += "!!"
			self.swipeTable.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
		}

		edit.backgroundColor = UIColor.greenColor()

		// 削除
		let del = UITableViewRowAction(style: .Default, title: "Delete") {
			(action, indexPath) in

			self.itemArray.removeAtIndex(indexPath.row)
			tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
		}

		del.backgroundColor = UIColor.redColor()

		return [edit, del]
	}

	////////////////////////////////////////////////////////////////
	// MARK: - Cellの表示

	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 1
	}

	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return itemArray.count
	}

	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell
		cell.textLabel?.text = itemArray[indexPath.row]
		return cell
	}

	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		tableView.deselectRowAtIndexPath(indexPath, animated: true)
	}
}
